import sys
import math
# own module(s)
import pntvec

def cylinderSides(vOffset, nOffset, num, p1, p2):
    rim = [] # to return the rim
    print "# cylinder sides at bottom of the lotus"
    nVertices = 0
    nNormals = 0
    # extract coords
    x1 = p1[0]
    y1 = p1[1]
    z1 = p1[2]
    x2 = p2[0]
    y2 = p2[1]
    z2 = p2[2]
    z = z2
    # angle between x-axis and p1
    rad = math.atan(y1 / x1)
    # how many points to intersection?
    num1 = int(num * (4.0 * rad) / math.pi)
    # how many points from intersection to second intersection?
    num2 = 2 * (num - num1) + 1
    # points on cylinder
    print "# points on cylinder"
    rad1 = math.atan(y1 / x1)
    rad2 = math.atan(y2 / x2)
    points = []
    for i in xrange(num2):
        rad = rad1 + i * (rad2 - rad1) / float(num2 - 1)
        cos = math.cos(rad)
        sin = math.sin(rad)
        x = 3.0 * cos
        y = 3.0 * sin
        print "v %s %s %s" % (x, y, z)
        nVertices = nVertices + 1
        points.append([x, y, z])
        # calculate points on cylinder (with z != 0.001)
        a = 1
        b = -5.0
        c = 8.25 - 2 * (x + y)
        discriminant = b * b - 4.0 * a * c
        z1 = (5.0 + math.sqrt(discriminant)) / 2.0
        z2 = (5.0 - math.sqrt(discriminant)) / 2.0
        if i != 0 and i != num2 - 1:
            print "v %s %s %s" % (x, y, z2)
            nVertices = nVertices + 1
            points.append([x, y, z2])
            rim.append([x, y, z2])
    # calculate vertex normals (inside)
    print "# vertex normals (point towards inside)"
    for point in points:
        v = pntvec.Vector(0.0 - point[0], 0.0 - point[1], 0.0)
        vn = v / v.length()
        x = vn[0]
        y = vn[1]
        z = vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    # triangle
    v = 1 + vOffset
    vn = 1 + nOffset
    print "f",
    print "%s/%s/%s" % (v, vt, vn),
    v = 2 + vOffset
    vn = 2 + nOffset
    print "%s/%s/%s" % (v, vt, vn),
    v = 3 + vOffset
    vn = 3 + nOffset
    print "%s/%s/%s" % (v, vt, vn),
    print ""
    for i in xrange(1, len(points) - 3, 2):
        # quad
        v = 1 + i + vOffset
        vn = 1 + i + nOffset
        print "f",
        print "%s/%s/%s" % (v, vt, vn),
        v = 3 + i + vOffset
        vn = 3 + i + nOffset
        print "%s/%s/%s" % (v, vt, vn),
        v = 4 + i + vOffset
        vn = 4 + i + nOffset
        print "%s/%s/%s" % (v, vt, vn),
        v = 2 + i + vOffset
        vn = 2 + i + nOffset
        print "%s/%s/%s" % (v, vt, vn),
        print ""
    # triangle
    v = len(points) - 2 + vOffset
    vn = len(points) - 2 + nOffset
    print "f",
    print "%s/%s/%s" % (v, vt, vn),
    v = len(points) - 0 + vOffset
    vn = len(points) - 0 + nOffset
    print "%s/%s/%s" % (v, vt, vn),
    v = len(points) - 1 + vOffset
    vn = len(points) - 1 + nOffset
    print "%s/%s/%s" % (v, vt, vn),
    print ""
    return vOffset + nVertices, nOffset + nNormals, rim

def cylinderTop(vOffset, nOffset, num, p1, p2):
    rim = [] # to return the rim
    print "# cylinder top at bottom of the lotus"
    nVertices = 0
    nNormals = 0
    # extract coords
    x1 = p1[0]
    y1 = p1[1]
    z1 = p1[2]
    x2 = p2[0]
    y2 = p2[1]
    z2 = p2[2]
    if z1 == z2:
        z = z1
    # angle between x-axis and p1
    rad = math.atan(y2 / x2)
    # how many points to intersection?
    num1 = int(num * (4.0 * rad) / math.pi)
    # how many points from intersection to 45 degrees?
    num2 = num - num1 + 1
    # points on ll_ne with z = 0.001
    k2 = 9.0 - 6.245001
    k = math.sqrt(k2)
    rad1 = math.atan((y1 - 1.0) / (x1 - 1.0))
    rad2 = math.atan((y2 - 1.0) / (x2 - 1.0))
    print "# center"
    print "v %s %s %s" % (0.0, 0.0, z) # write center once
    nVertices = nVertices + 1
    print "# points on sphere"
    for i in xrange(num1):
        rad = rad1 + i * (rad2 - rad1) / float(num1 - 1)
        cos = math.cos(rad)
        sin = math.sin(rad)
        x = 1.0 + k * cos
        y = 1.0 + k * sin
        print "v %s %s %s" % (x, y, z)
        nVertices = nVertices + 1
        rim.append([x, y, z])
    print "# points on cylinder"
    rad1 = math.atan(y2 / x2)
    rad2 = math.pi / 4.0
    for i in xrange(1, num2): # omit first vertex (was written already above)
        rad = rad1 + i * (rad2 - rad1) / float(num2 - 1)
        cos = math.cos(rad)
        sin = math.sin(rad)
        x = 3.0 * cos
        y = 3.0 * sin
        print "v %s %s %s" % (x, y, z)
        nVertices = nVertices + 1
    # share vertex normal
    x = 0.0
    y = 0.0
    z = -1.0
    print "# share vertex normal"
    print "vn %s %s %s" % (x, y, z)
    nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    vn = 1 + nOffset
    for i in xrange(1, nVertices - 1):
        v = 1 + vOffset
        print "f",
        print "%s/%s/%s" % (v, vt, vn),
        v = 2 + i + vOffset
        print "%s/%s/%s" % (v, vt, vn),
        v = 1 + i + vOffset
        print "%s/%s/%s" % (v, vt, vn),
        print ""
    return vOffset + nVertices, nOffset + nNormals, rim

def bottom(num, vOffset, nOffset):
    z = 0.001
    # intersection point between ll_se and ll_ne
    a = 1
    b = -2
    c = -0.754999
    discriminant = b * b - 4.0 * a * c
    x1 = (2 + math.sqrt(discriminant)) / 2.0
    y1 = 0.0
    p1 = [x1, y1, z] # we need only one of them
    # intersection point between clip_base_m and ll_ne
    m = (z * z - 5 * z + 8.25) / 2.0
    a = 2
    b = -2 * m
    c = m * m - 9
    discriminant = b * b - 4.0 * a * c
    x2 = (2 * m + math.sqrt(discriminant)) / 4.0
    x3 = (2 * m - math.sqrt(discriminant)) / 4.0
    y2 = m - x2
    y3 = m - x3
    p2 = [x2, y2, z]
    p3 = [x3, y3, z]
    # cylinder top at bottom of the lotus
    vOffset, nOffset, part1 = cylinderTop(vOffset, nOffset, num, p1, p2)
    # cylinder sides at bottom of the lotus
    vOffset, nOffset, part2 = cylinderSides(vOffset, nOffset, num, p2, p3)
    # assemble parts for lower rim
    points = []
    for point in part1:
        # simply copy
        points.append(point)
    for point in part2:
        # simply copy
        points.append(point)
    for index in xrange(len(part1)):
        rIndex = len(part1) - 1 - index
        point = part1[rIndex] # reverse order
        # swap x and y
        points.append([point[1], point[0], point[2]])
    return vOffset, nOffset, points

def outerShell(num, vOffset, nOffset, lowerRim):
    rim = [] # to return the rim
    print "# outer shell"
    nVertices = 0
    nNormals = 0
    # calculate upper points
    for i in xrange(len(lowerRim)):
        rad = i * 0.5 * math.pi / float(len(lowerRim) - 1)
        cos = math.cos(rad)
        sin = math.sin(rad)
        a = 1.0 + (1.0 / 9.0) * (1.0 + 2.0 * sin * cos)
        b = -2.0 * (cos + sin)
        c = -7.0
        discriminant = b * b - 4.0 * a * c
        k1 = (-b + math.sqrt(discriminant)) / (2.0 * a)
        k2 = (-b - math.sqrt(discriminant)) / (2.0 * a)
        if k1 < 0.0:
            k = k2
        else:
            k = k1
        x = k * cos
        y = k * sin
        z = 2.5 + (k / 3.0) * (cos + sin)
        rim.append([x, y, z])
    # connect upper and lower rim by calculating the points inbetween
    print "# points on sphere"
    points = []
    for index in xrange(len(rim)):
        p1 = rim[index]
        p2 = lowerRim[index]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not(i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 2.5)
                vn = v / v.length()
                c = pntvec.Vector(1.0, 1.0, 2.5 - 2.5)
                discriminant = (vn * c) * (vn * c) - c * c + 9.0
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 2.5
            print "v %s %s %s" % (x, y, z)
            points.append([x, y, z])
            nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(1.0 - point[0], 1.0 - point[1], 2.5 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(rim) - 1):
        for i in xrange(num - 1):
            # quad
            v = 1 + i + index * num + vOffset
            vn = 1 + i + index * num + nOffset
            print "f",
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + vOffset
            vn = 2 + i + index * num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + num + vOffset
            vn = 2 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 1 + i + index * num + num + vOffset
            vn = 1 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            print ""
    return vOffset + nVertices, nOffset + nNormals, rim

def middleShell(num, vOffset, nOffset):
    lowerRim = [] # to return the lower rim
    upperRim = [] # to return the upper rim
    print "# outer shell"
    nVertices = 0
    nNormals = 0
    # intersection point in lower right quadrant
    xc = 0.75 * math.sqrt(2)
    # I  (x - xc)^2 + y^2 + (z - 2.75)^2 = 6.25
    # II x^2 + y^2 + (z - 5.5)^2 = 16
    # I-II -2*xc*x + xc^2 + 5.5*z - 12.9375 = 0
    k1 = (2 * xc) / 5.5
    k2 = (12.9375 - xc * xc) / 5.5
    # => z = k1*x + k2
    # III x = -y
    # II' 2*x^2 + (z - 5.5)^2 = 16
    # substitute z
    # 2*x^2 + (k1^2*x^2 + 2*k1*k2*x + k2^2) - 11*k1*x - 11*k2 + 30.25 = 16
    a = 2 + k1 * k1
    b = 2*k1*k2- 11*k1
    c = k2 * k2 - 11 * k2 + 30.25 - 16
    discriminant = b * b - 4.0 * a * c
    x1 = (-b + math.sqrt(discriminant)) / (2.0 * a)
    y1 = -x1
    # intersection point in upper right quadrant
    x2 = x1
    y2 = x2
    # calculate lower points
    rad1 = -math.pi / 4.0
    rad2 = math.pi / 4.0
    for i in xrange(2*num - 1):
        rad = rad1 + i * (rad2 - rad1) / float(2*num - 2)
        cos = math.cos(rad)
        sin = math.sin(rad)
        # III x = k3 * cos; y = k3 * sin
        # II' k3^2*cos^2 + k3^2*sin^2 + (z - 5.5)^2 = 16
        a = 1 + k1 * k1 * cos * cos
        b = 2 * k1 * k2 * cos - 11 * k1 * cos
        c = k2 * k2 - 11 * k2 + 30.25 - 16
        discriminant = b * b - 4.0 * a * c
        k3 = (-b + math.sqrt(discriminant)) / (2.0 * a)
        x = k3 * cos
        y = k3 * sin
        z = k1 * x + k2 # see above
        lowerRim.append([x, y, z])
    # intersection point in lower right quadrant
    xc = 0.75 * math.sqrt(2)
    # I  (x - xc)^2 + y^2 + (z - 2.75)^2 = 6.25
    # II x^2 + y^2 + (z - 5.75)^2 = 9
    # I-II -2*xc*x + xc^2 + 6*z - 22.75 = 0
    k1 = (2 * xc) / 6.0
    k2 = (22.75 - xc * xc) / 6.0
    # => z = k1*x + k2
    # III x = -y
    # II' 2*x^2 + (z - 5.75)^2 = 9
    # substitute z
    # 2*x^2 + (k1^2*x^2 + 2*k1*k2*x + k2^2) - 11.5*k1*x - 11.5*k2 + 33.0625 = 9
    a = 2 + k1 * k1
    b = 2*k1*k2- 11.5*k1
    c = k2 * k2 - 11.5 * k2 + 33.0625 - 9
    discriminant = b * b - 4.0 * a * c
    x1 = (-b + math.sqrt(discriminant)) / (2.0 * a)
    y1 = -x1
    # intersection point in upper right quadrant
    x2 = x1
    y2 = x2
    # calculate upper points
    rad1 = -math.pi / 4.0
    rad2 = math.pi / 4.0
    for i in xrange(2*num - 1):
        rad = rad1 + i * (rad2 - rad1) / float(2*num - 2)
        cos = math.cos(rad)
        sin = math.sin(rad)
        # III x = k3 * cos; y = k3 * sin
        # II' k3^2*cos^2 + k3^2*sin^2 + (z - 5.75)^2 = 9
        a = 1 + k1 * k1 * cos * cos
        b = 2 * k1 * k2 * cos - 11.5 * k1 * cos
        c = k2 * k2 - 11.5 * k2 + 33.0625 - 9
        discriminant = b * b - 4.0 * a * c
        k3 = (-b + math.sqrt(discriminant)) / (2.0 * a)
        x = k3 * cos
        y = k3 * sin
        z = k1 * x + k2 # see above
        upperRim.append([x, y, z])
    # connect upper and lower rim by calculating the points inbetween
    print "# points on sphere"
    points = []
    for index in xrange(len(upperRim)):
        p1 = upperRim[index]
        p2 = lowerRim[index]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not (i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 2.75)
                vn = v / v.length()
                c = pntvec.Vector(xc, 0.0, 2.75 - 2.75)
                discriminant = (vn * c) * (vn * c) - c * c + 6.25
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 2.75
            print "v %s %s %s" % (x, y, z)
            points.append([x, y, z])
            nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(1.0 - point[0], 1.0 - point[1], 2.5 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(upperRim) - 1):
        for i in xrange(num - 1):
            # quad
            v = 1 + i + index * num + vOffset
            vn = 1 + i + index * num + nOffset
            print "f",
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + vOffset
            vn = 2 + i + index * num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + num + vOffset
            vn = 2 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 1 + i + index * num + num + vOffset
            vn = 1 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            print ""
    # take the second half first
    newLowerRim = []
    newUpperRim = []
    length = len(lowerRim)
    for index in xrange(length/2, length):
        low = lowerRim[index]
        up = upperRim[index]
        # simply copy
        newLowerRim.append(low)
        newUpperRim.append(up)
    # the first half (missing the point in the middle) gets transformed
    for index in xrange(length/2):
        low = lowerRim[index+1]
        up = upperRim[index+1]
        # rotate by 90 degrees
        x = -low[1]
        y = low[0]
        z = low[2]
        newLowerRim.append([x, y, z])
        x = -up[1]
        y = up[0]
        z = up[2]
        newUpperRim.append([x, y, z])
    return vOffset + nVertices, nOffset + nNormals, newLowerRim, newUpperRim

def outerTop(num, vOffset, nOffset, lowerRim, upperRim):
    print "# outer top"
    nVertices = 0
    nNormals = 0
    # connect upper and lower rim by calculating the points inbetween
    print "# points on sphere"
    points = []
    for index in xrange(len(upperRim)):
        p1 = upperRim[index]
        p2 = lowerRim[index]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not(i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 5.5)
                vn = v / v.length()
                c = pntvec.Vector(0.0, 0.0, 5.5 - 5.5)
                discriminant = (vn * c) * (vn * c) - c * c + 16.0
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 5.5
            print "v %s %s %s" % (x, y, z)
            points.append([x, y, z])
            nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(1.0 - point[0], 1.0 - point[1], 2.5 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(upperRim) - 1):
        for i in xrange(num - 1):
            # quad
            v = 1 + i + index * num + vOffset
            vn = 1 + i + index * num + nOffset
            print "f",
            print "%s/%s/%s" % (v, vt, vn),
            v = 1 + i + index * num + num + vOffset
            vn = 1 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + num + vOffset
            vn = 2 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + vOffset
            vn = 2 + i + index * num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            print ""
    return vOffset + nVertices, nOffset + nNormals

def middleTop(num, vOffset, nOffset, lowerRim, upperRim):
    print "# outer top"
    nVertices = 0
    nNormals = 0
    # connect upper and lower rim by calculating the points inbetween
    print "# points on sphere"
    points = []
    for index in xrange(len(upperRim)):
        p1 = upperRim[index]
        p2 = lowerRim[index]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not(i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 5.75)
                vn = v / v.length()
                c = pntvec.Vector(0.0, 0.0, 5.75 - 5.75)
                discriminant = (vn * c) * (vn * c) - c * c + 9.0
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 5.75
            print "v %s %s %s" % (x, y, z)
            points.append([x, y, z])
            nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(0.0 - point[0], 0.0 - point[1], 5.75 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(upperRim) - 1):
        for i in xrange(num - 1):
            # quad
            v = 1 + i + index * num + vOffset
            vn = 1 + i + index * num + nOffset
            print "f",
            print "%s/%s/%s" % (v, vt, vn),
            v = 1 + i + index * num + num + vOffset
            vn = 1 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + num + vOffset
            vn = 2 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + vOffset
            vn = 2 + i + index * num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            print ""
    return vOffset + nVertices, nOffset + nNormals

def innerTop(num, vOffset, nOffset, upperRim):
    print "# inner top"
    nVertices = 0
    nNormals = 0
    print "# center"
    print "v %s %s %s" % (0.0, 0.0, 4.2 - 1.8) # write center once
    nVertices = nVertices + 1
    # connect rim and center by calculating the points inbetween
    print "# points on sphere"
    points = [[0.0, 0.0, 4.2 - 1.8]]
    for index in xrange(len(upperRim)):
        p1 = upperRim[index]
        p2 = [0.0, 0.0, 4.2 - 1.8]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not(i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 4.2)
                vn = v / v.length()
                c = pntvec.Vector(0.0, 0.0, 4.2 - 4.2)
                discriminant = (vn * c) * (vn * c) - c * c + 3.24
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 4.2
            if i != num - 1: # don't write center
                print "v %s %s %s" % (x, y, z)
                points.append([x, y, z])
                nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(0.0 - point[0], 0.0 - point[1], 4.2 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(upperRim) - 1):
        for i in xrange(num - 1):
            if i == num - 2:
                # triangle
                v = 2 + i + index * (num - 1) + vOffset
                vn = 2 + i + index * (num - 1) + nOffset
                print "f",
                print "%s/%s/%s" % (v, vt, vn),
                v = 2 + i + index * (num - 1) + (num - 1) + vOffset
                vn = 2 + i + index * (num - 1) + (num - 1) + nOffset
                print "%s/%s/%s" % (v, vt, vn),
                v = 1 + vOffset  # center
                vn = 1 + nOffset # center
                print "%s/%s/%s" % (v, vt, vn),
                print ""
            else:
                # quad
                v = 2 + i + index * (num - 1) + vOffset
                vn = 2 + i + index * (num - 1) + nOffset
                print "f",
                print "%s/%s/%s" % (v, vt, vn),
                v = 2 + i + index * (num - 1) + (num - 1) + vOffset
                vn = 2 + i + index * (num - 1) + (num - 1) + nOffset
                print "%s/%s/%s" % (v, vt, vn),
                v = 3 + i + index * (num - 1) + (num - 1) + vOffset
                vn = 3 + i + index * (num - 1) + (num - 1) + nOffset
                print "%s/%s/%s" % (v, vt, vn),
                v = 3 + i + index * (num - 1) + vOffset
                vn = 3 + i + index * (num - 1) + nOffset
                print "%s/%s/%s" % (v, vt, vn),
                print ""
    return vOffset + nVertices, nOffset + nNormals

def innerShell(num, vOffset, nOffset):
    lowerRim = [] # to return the lower rim
    upperRim = [] # to return the upper rim
    print "# outer shell"
    nVertices = 0
    nNormals = 0
    # intersection point in lower right quadrant
    # I  (x - 0.45)^2 + (y - 0.45)^2 + (z - 2.4)^2 = 2.25
    # II x^2 + y^2 + (z - 5.75)^2 = 9
    # I-II -0.9*x - 0.9*y + 6.7*z - 20.1475 = 0
    # III y = 0
    k1 = 0.9 / 6.7
    k2 = 20.1475 / 6.7
    # => z = k1*x + k2
    # II' x^2 + (z - 5.75)^2 = 9
    # substitute z
    # x^2 + k1^2*x^2 + 2*k1*k2*x + k2^2 - 11.5*k1*x - 11.5*k2 + 24.0625 = 0
    a = 1 + k1 * k1
    b = 2 * k1 * k2 - 11.5 * k1
    c = k2 * k2 - 11.5 * k2 + 24.0625
    discriminant = b * b - 4.0 * a * c
    x1 = (-b + math.sqrt(discriminant)) / (2.0 * a)
    y1 = 0.0
    z1 = k1 * x1 + k2
    # intersection point in upper right quadrant
    x2 = 0.0
    y2 = x1
    z2 = k1 * y2 + k2
    # calculate lower points
    rad1 = 0.0
    rad2 = math.pi / 2.0
    for i in xrange(2*num - 1):
        rad = rad1 + i * (rad2 - rad1) / float(2*num - 2)
        cos = math.cos(rad)
        sin = math.sin(rad)
        # III x = k3 * cos; y = k3 * sin
        # II' k3^2*cos^2 + k3^2*sin^2 + (z - 5.75)^2 = 9
        # z^2 = 20.1475 * (20.1475 + 0.9*k3*cos + 0.9*k3*sin) / (6.7 * 6.7) +
        # 0.9*k3*cos * (20.1475 + 0.9*k3*cos + 0.9*k3*sin) / (6.7 * 6.7) + 
        # 0.9*k3*sin * (20.1475 + 0.9*k3*cos + 0.9*k3*sin) / (6.7 * 6.7) + 
        # -11.5*z = -11.5 * (20.1475 + 0.9*k3*cos + 0.9*k3*sin) / 6.7
        # 33.0625 - 9 = 0
        a = 1 + 2*0.9*0.9*sin*cos/(6.7*6.7) + 0.9*0.9/(6.7*6.7)
        b = 20.1475*(2*0.9*cos+2*0.9*sin)/(6.7*6.7) - 11.5*(0.9*cos+0.9*sin)/6.7
        c = 20.1475*20.1475/(6.7*6.7) - 11.5*20.1475/6.7 + 33.0625 - 9
        discriminant = b * b - 4.0 * a * c
        k3 = (-b + math.sqrt(discriminant)) / (2.0 * a)
        x = k3 * cos
        y = k3 * sin
        z = (20.1475 + 0.9*x + 0.9*y) / 6.7 # see above
        lowerRim.append([x, y, z])
    # intersection point in lower right quadrant
    # I  (x - 0.45)^2 + (y - 0.45)^2 + (z - 2.4)^2 = 2.25
    # II x^2 + y^2 + (z - 4.2)^2 = 1.8^2
    # I-II -0.9*x - 0.9*y + 3.6*z - 10.485 = 0
    # III y = 0
    k1 = 0.9 / 3.6
    k2 = 10.485 / 3.6
    # => z = k1*x + k2
    # II' x^2 + (z - 4.2)^2 = 3.24
    # substitute z
    # x^2 + k1^2*x^2 + 2*k1*k2*x + k2^2 - 8.4*k1*x - 8.4*k2 + 14.4 = 0
    a = 1 + k1 * k1
    b = 2 * k1 * k2 - 8.4 * k1
    c = k2 * k2 - 8.4 * k2 + 14.4
    discriminant = b * b - 4.0 * a * c
    x1 = (-b + math.sqrt(discriminant)) / (2.0 * a)
    y1 = 0.0
    z1 = k1 * x1 + k2
    # intersection point in upper right quadrant
    x2 = 0.0
    y2 = x1
    z2 = k1 * y2 + k2
    # calculate upper points
    rad1 = 0.0
    rad2 = math.pi / 2.0
    for i in xrange(2*num - 1):
        rad = rad1 + i * (rad2 - rad1) / float(2*num - 2)
        cos = math.cos(rad)
        sin = math.sin(rad)
        # III x = k3 * cos; y = k3 * sin
        # II' k3^2*cos^2 + k3^2*sin^2 + (z - 4.2)^2 = 3.24
        # z^2 = 10.485 * (10.485 + 0.9*k3*cos + 0.9*k3*sin) / (3.6 * 3.6) +
        # 0.9*k3*cos * (10.485 + 0.9*k3*cos + 0.9*k3*sin) / (3.6 * 3.6) + 
        # 0.9*k3*sin * (10.485 + 0.9*k3*cos + 0.9*k3*sin) / (3.6 * 3.6) + 
        # -8.4*z = -8.4 * (10.485 + 0.9*k3*cos + 0.9*k3*sin) / 3.6
        # 17.64 - 3.24 = 0
        a = 1 + 2*0.9*0.9*sin*cos/(3.6*3.6) + 0.9*0.9/(3.6*3.6)
        b = 10.485*(2*0.9*cos+2*0.9*sin)/(3.6*3.6) - 8.4*(0.9*cos+0.9*sin)/3.6
        c = 10.485*10.485/(3.6*3.6) - 8.4*10.485/3.6 + 17.64 - 3.24
        discriminant = b * b - 4.0 * a * c
        k3 = (-b + math.sqrt(discriminant)) / (2.0 * a)
        x = k3 * cos
        y = k3 * sin
        z = (10.485 + 0.9*x + 0.9*y) / 3.6 # see above
        upperRim.append([x, y, z])
    # connect upper and lower rim by calculating the points inbetween
    print "# points on sphere"
    points = []
    for index in xrange(len(upperRim)):
        p1 = upperRim[index]
        p2 = lowerRim[index]
        for i in xrange(num):
            factor = i / float(num - 1)
            x = (1.0 - factor) * p1[0] + factor * p2[0]
            y = (1.0 - factor) * p1[1] + factor * p2[1]
            z = (1.0 - factor) * p1[2] + factor * p2[2]
            if not (i == 0 or i == num - 1):
                # correct values
                v = pntvec.Vector(x, y, z - 2.4)
                vn = v / v.length()
                c = pntvec.Vector(0.45, 0.45, 2.4 - 2.4)
                discriminant = (vn * c) * (vn * c) - c * c + 2.25
                d1 = (vn * c) + math.sqrt(discriminant)
                d2 = (vn * c) - math.sqrt(discriminant)
                s = vn * d1
                x = s[0]
                y = s[1]
                z = s[2] + 2.4
            print "v %s %s %s" % (x, y, z)
            points.append([x, y, z])
            nVertices = nVertices + 1
    # calculate vertex normals (outside)
    print "# vertex normals (point towards outside)"
    for point in points:
        v = pntvec.Vector(1.0 - point[0], 1.0 - point[1], 2.5 - point[2])
        vn = v / v.length()
        x = -vn[0]
        y = -vn[1]
        z = -vn[2]
        print "vn %s %s %s" % (x, y, z)
        nNormals = nNormals + 1
    # write faces
    print "# faces"
    vt = ""
    for index in xrange(len(upperRim) - 1):
        for i in xrange(num - 1):
            # quad
            v = 1 + i + index * num + vOffset
            vn = 1 + i + index * num + nOffset
            print "f",
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + vOffset
            vn = 2 + i + index * num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 2 + i + index * num + num + vOffset
            vn = 2 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            v = 1 + i + index * num + num + vOffset
            vn = 1 + i + index * num + num + nOffset
            print "%s/%s/%s" % (v, vt, vn),
            print ""
    return vOffset + nVertices, nOffset + nNormals, lowerRim, upperRim

if __name__ == "__main__":
    num = 16
    vOffset = 0
    nOffset = 0
    # bottom (45 degrees)
    vOffset, nOffset, lowerRim = bottom(num, vOffset, nOffset)
    # outer shell (90 degrees)
    vOffset, nOffset, upperRim = outerShell(num, vOffset, nOffset, lowerRim)
    # middle shell (90 degrees)
    vOffset, nOffset, lmRim, umRim = middleShell(num, vOffset, nOffset)
    # outer top
    vOffset, nOffset = outerTop(num, vOffset, nOffset, lmRim, upperRim)
    # inner shell (90 degrees)
    vOffset, nOffset, liRim, uiRim = innerShell(num, vOffset, nOffset)
    # middle top
    vOffset, nOffset = middleTop(num, vOffset, nOffset, liRim, umRim)
    # inner top
    vOffset, nOffset = innerTop(num, vOffset, nOffset, uiRim)
