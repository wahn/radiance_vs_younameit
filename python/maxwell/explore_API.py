#!/usr/bin/python2.7

__author__ = 'jan'

import sys
try:
    import pymaxwell
except ImportError:
    sys.path.append("/Applications/Maxwell 3/Libs/pymaxwell/python2.7")
    # try again
    import pymaxwell

# scene
scene = pymaxwell.Cmaxwell(pymaxwell.mwcallback)
# camera
pName = 'camera'
nSteps = 1
shutter = 1/250.0
filmWidth = 0.04
filmHeight = 0.035
iso = 400
pDiaphragmType = 'CIRCULAR'
angle = 0.0
nBlades = 0
fps = 25
xRes = 500
yRes = 500
pixelAspect = 1.0
camera = scene.addCamera(pName, nSteps, shutter, filmWidth, filmHeight,
                         iso, pDiaphragmType, angle, nBlades, fps,
                         xRes, yRes, pixelAspect)
iStep = 0
origin = pymaxwell.Cvector(0.0, 0.0, 3.0)
focalPoint = pymaxwell.Cvector(0.0, 0.0, 0.0)
up = pymaxwell.Cvector(0.0, 0.0, 1.0)
focalLength = 0.035
fStop = 8.0
focalLengthNeedCorrection = True
camera.setStep(iStep, origin, focalPoint, up,
               focalLength, fStop, focalLengthNeedCorrection)
camera.setActive()
# mesh
pName = 'mesh'
nVertexes = 4
nNormals = 1
nTriangles = 2
nPositionsPerVertexes = 1 # 1 for no motion blur
mesh = scene.createMesh(pName, nVertexes, nNormals, nTriangles,
                        nPositionsPerVertexes)
mesh.setVertex(0, 0, pymaxwell.Cvector(-1.0, -1.0, -1.0))
mesh.setVertex(1, 0, pymaxwell.Cvector( 1.0, -1.0, -1.0))
mesh.setVertex(2, 0, pymaxwell.Cvector( 1.0, -1.0,  1.0))
mesh.setVertex(3, 0, pymaxwell.Cvector(-1.0, -1.0,  1.0))
mesh.setNormal(0, 0, pymaxwell.Cvector(0.0, 1.0, 0.0))
mesh.setTriangle(0, 0, 1, 2, 0, 0, 0)
mesh.setTriangle(1, 0, 2, 3, 0, 0, 0)
# light emitting mesh
pName = 'light'
nVertexes = 4
nNormals = 1
nTriangles = 2
nPositionsPerVertexes = 1
light = scene.createMesh(pName, nVertexes, nNormals, nTriangles,
                         nPositionsPerVertexes)
light.setVertex(0, 0, pymaxwell.Cvector(-1.0, 1.0, -1.0))
light.setVertex(1, 0, pymaxwell.Cvector( 1.0, 1.0, -1.0))
light.setVertex(2, 0, pymaxwell.Cvector( 1.0, 1.0,  1.0))
light.setVertex(3, 0, pymaxwell.Cvector(-1.0, 1.0,  1.0))
light.setNormal(0, 0, pymaxwell.Cvector(0.0, -1.0, 0.0))
light.setTriangle(0, 0, 1, 2, 0, 0, 0)
light.setTriangle(1, 0, 2, 3, 0, 0, 0)
# material
readMaterial = scene.readMaterial("/Applications/Maxwell 3/materials database/mxm files/default.mxm")
material = scene.addMaterial(readMaterial)
mesh.setMaterial(material)
# emitter
emitter_mat = scene.createMaterial('emitter_mat', True)
layer = emitter_mat.addLayer()
layer.setEnabled(True)
emitter = layer.createEmitter()
emitter.setLobeType(pymaxwell.EMISSION_LOBE_DEFAULT)
emitter.setActiveEmissionType(pymaxwell.EMISSION_TYPE_PAIR)
emitterPair = pymaxwell.CemitterPair()
emitterPair.watts = 1000.0
emitterPair.luminousEfficacy = 100.0
emitter.setPair(emitterPair)
emitter.setState(True)
light.setMaterial(emitter_mat)
scene.writeMXS("explore_API.mxs")
