\documentclass[12pt,oneside,a4paper]{article}
\usepackage{hyperref,graphicx,fancyhdr}
\pagestyle{fancy}
\lhead{Radiance vs. YouNameIt}
\rhead{page \thepage}
\cfoot{Jan Walter -- The Mill}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\begin{document}

\title{Radiance vs. YouNameIt}
\author{Jan Walter}
\date{\today}
\maketitle

\section{Introduction}

\subsection{What is Radiance?}

``Radiance is a suite of tools for performing lighting simulation
originally written by Greg Ward.'' --- (see
Wikipedia\footnote{\url{http://en.wikipedia.org/wiki/Radiance_(software)}}
article).

\subsection{Other Renderers}

In this article I will take scenes which were originally modelled for
Radiance and convert them, so they can be used to render images with
other renderers.

\section{Simple Room}

Probably the simplest scene one could start with would be two
spheres. One is visible to the camera, the other one emits light, so
we can see something (in this case the first sphere). This is actually
how chapter 1.3.1 of the {\bfseries Rendering with Radiance}
book\footnote{\url{http://radsite.lbl.gov/radiance/book/index.html}}
begins. I highly recomment to get hold of a copy of the book and to
install the software to be able to experiment on your own, but the
link mentioned in the footnote of the Radiance book contains a
PDF\footnote{\url{http://radsite.lbl.gov/radiance/book/ch1/ch1.pdf}}
file, which gets you started without having access to the book itself.

At the end of the chapter 1.3 you will have created a simple room,
which I will use as an introduction to several concepts which you will
find in other renderers as well.

You might be familiar with point lights in some \underline{D}igital
\underline{C}ontent \underline{C}reation (DCC) applications and other
renderers. The first concept I want to talk about here is {\bfseries
  light emitting geometry}. Even though some (global illumination)
renderers might still support point lights, which do not have any
shape, and are conceptionally just positions in space where light
emits from, Radiance uses in this scene a sphere (with a radius, so
it's visible) and the light\footnote{See
  \url{http://radsite.lbl.gov/radiance/refer/ray.html} for an intro of
  the Radiance scene description.} material for self-luminous
surfaces.

\begin{figure}[h]
\centering
\includegraphics[scale=0.48]{img/Simple_Room_800x538.png}
\caption{Close--up vs. four different camera positions.}
\label{fig:simple_room_close_up}
\end{figure}

In the figure \ref{fig:simple_room_close_up} we see a closeup of a
crystal sphere on the left side, where the rest of the scene is
reflected. On the right I rendered the same scene from four different
camera perspectives to show that there is not only a room, but also an
outside world.

The crystal sphere rests on a blue box, which acts as a table. The
room itself has four walls (with the same material), a ceiling (which
is slightly brighter), and a brown floor. One of the walls has an
opening with a glass window. Through that window light from outside
enters the room, casts some bright light onto the floor (and part of
one wall), visible only in the reflection of the crystal sphere.

If we place a camera just outside that room and look through the
window, we will see that the light emitting sphere is mounted to the
ceiling with a chrome cylinder. It casts a shadow\footnote{If the
  shadow would really come from a spherical light source, the shadow
  would not be that sharp.} of the blue box against one of the
walls. Another (softer) shadow results from a {\bfseries sun and sky
  simulation} outside the room. Light from the sky would fall through
the window and the much brighter light from the sun would bounce
around the room and soften the shadows.

If we zoom out even more by placing the camera above the room and
further away we will see that the sun light casts another shadow onto
a ground disk. The sun and sky simulation also draws a ground color at
the horizon, beside painting the background with a sky color. If you
would look at the alpha channel of the rendered image you would see
the lower part as white (geometry got hit) and the upper part as black
(no geometry got hit, the resulting colors come from the sun and sky
shader).

Finally we place the camera even further away to see a reflective
building (just a box), which reflects the sun light and creates a
brighter spot on the ground disk, which in some renderers can be
simulated by using {\bfseries caustics}.

\subsection{Importers}

The main idea of comparing Radiance with other renderers is to import
the geometry and the used materials, lights, and cameras into a freely
available DCC application (in this case {\bfseries Blender}) in a way
that most of the original data can be stored and exported again
without manually adjusting the scene.

\subsubsection{Naming Convention}

In this regard we introduce a {\bfseries naming convention} which can
be picked up by the exporters to keep {\bfseries primitives}, if they
are supported by the renderer in question.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{img/rvu_blend_outliner_combine_geo.png}
\caption{Naming convention and combining geometry.}
\label{fig:naming_convention_combined_geo}
\end{figure}

\subsubsection{Multiple Materials}

But it makes sense to {\bfseries combine} for example {\bfseries
  geometry} which can be looked at as a unit, e.g. a room, instead of
having the ceiling, the floor, and all the walls in separate
objects. Blender does allow this, as you can see in the annotated
screenshot of figure
\ref{fig:naming_convention_combined_geo}. Materials are per face and a
single mesh can contain multiple materials. Obviously the exporters
have to deal with that, some renderers might force you to separate the
geometry again into objects which have a single shader attached,
others allow you to index per face into a shader list, so that a mesh
with several materials can be exported as a single entity.

\subsubsection{Changing the Tesselation}

\begin{figure}[h]
\centering
\includegraphics[scale=0.645]{img/rvu_blend_simplify_geo_03.png}
\caption{Simplified geometry.}
\end{figure}

Because Radiance supports polygons which can have {\bfseries holes},
the resulting tesselation might be bad (or poor) and can be simplified
manually for other renderers. Sometimes you get clues about other
problems in e.g. meshes during rendering and it is a good idea to fix
those in Blender (or somewhere else) before exporting to other
renderers.

I don't want to show all these canges (to the original scene data) in
detail, but I want to mention (and therefore document them) on a per
scene basis.

\subsubsection{Window Glass}

Very often the scenes coming from Radiance use a single polygon with a
{\bfseries glass material}\footnote{See
  \url{http://radsite.lbl.gov/radiance/refer/ray.html}}, which is
optimized for thin glass surfaces. For most other renderers it's
better to use a real volume (or closed object) --- like a box --- with
outwards pointing normals.

As you can see in the screenshot below, I keep the original Radiance
scene description files, and the modifications I make (before
importing them into Blender) in a repository, which allows me to see
the differences.

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{img/rvu_rad_diff_window_glass.png}
\caption{Using Bitbucket's diff tool.}
\end{figure}

It's still worth to double check the normals (for objects using a
glass like material --- basically forcing the renderer to use
refraction rays) after importing the geometry. This will avoid strange
{\bfseries artefacts} with some renderers in case the normals are
{\bfseries not} pointing outwards.

\subsubsection{Light Emitting Spheres}

As I mentioned before in a footnote, Radiance allows to use light
emitting spheres, but the {\bfseries shadow calculation} is a bit
odd. For other renderers which support light emitting geometry, the
resulting shadows would be softer at the edges. Because light get's
emitted by a sphere with a radius and therefore several shadow rays
from different origins would soften the shadows at the edges.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{img/rvu_blend_fixture_compare.png}
\caption{Shadows cast by original vs. shortened cylinder.}
\label{fig:shadows_of_shorter_cylinder}
\end{figure}

In Radiance the light is still emitted from the center of the sphere
(like in a point light), but the sphere would not block the light
rays, otherwise no light could escape the sphere. The sphere would
also be visible from outside the sphere radius, and therefore it shows
up in reflections or if viewed directly.

This results in another modification I made to the original Radiance
scene data as illustrated in figure
\ref{fig:shadows_of_shorter_cylinder}. On the upper left you see a
cylinder which connects the light emitting sphere to the ceiling, but
the cylinder extends from the ceiling into the sphere middle, which
would cause shadows like in the rendering on the lower left.

What Radiance seems to do is to cut the part which is inside the light
emitting sphere away, so the resulting shadows on the ceiling look
like they would be caused by the remaining cylinder.

I didn't really calculate the proper remaining cylinder part, but what
I did was to use a cylinder which isn't that long, and the result can
be seen on the bottom right, using a point light, instead of a light
emitting sphere. We will come back to that later, because sometimes
you can use the same trick to render faster, or get rid of some noise.

\subsection{Exporters}

Of course you can write an exporter for each renderer individually,
and that's what I've done in the beginning, but at some point I
started to integrate as many renderers as I could into a single
exporter. Currently I support the following renderers:

\begin{itemize}
\item Arnold\footnote{\url{http://www.solidangle.com/}}
\item Indigo\footnote{\url{http://www.indigorenderer.com/}}
\item Luxrender\footnote{\url{http://www.luxrender.net/en_GB/index}}
\item mental ray\footnote{\url{http://www.nvidia-arc.com/mentalray.html}}
\item 3Delight\footnote{\url{http://www.3delight.com/en/index.php}}
  (RenderMan compliant)
\end{itemize}

\subsubsection{Shading Languages}

I started with Arnold and mental ray, because those are the main two
renderers we use in production at The Mill. I added a RenderMan
compatible renderer (3Delight), and all three of them support a
{\bfseries shading language}. I wanted to write at least some shaders
to see how certain concepts are implemented in different renderers
(for example AOVs --- \underline{A}rbitrary \underline{O}utput
\underline{V}ariables).

\subsubsection{Beauty, Alpha, and Depth}

Nearly all renderers can output an {\bfseries alpha channel} beside
the so--called {\bfseries beauty pass}, which is the image we want to
render. This is the simplest start to support compositing. The alpha
channel is pure white where it hits geometry, and pure black where it
doesn't. Because most pixels will be calculated by several samples it
can happen that we get a grey value. Basically this allows us to add
two images, the computer generated (for the foreground), and for
example a photograph (for the background).

Far less common is the {\bfseries depth channel}, which you might be
familiar with if your background is from a RenderMan compatible
renderer. The screenshot in figure \ref{fig:aovs_beauty_alpha_depth}
shows from left to right: beauty, alpha, depth.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_beauty_alpha_depth.png}
\caption{Beauty, alpha, and depth.}
\label{fig:aovs_beauty_alpha_depth}
\end{figure}

In principle this introduces {\bfseries coordinate systems}, because
the depth channel can be interpeted as the z-coordinate in a camera
coordinate system (where the x-axis goes left or right, the y-axis up
or down, and the z-axis into the image, along a pixel, or behind it).

One important detail about depth channels is that anti-aliasing
(several samples per pixel) doesn't make sense for such an image. Per
pixel there should be only one depth value, interpolating several
samples is in this case counterproductive. So, if you really want to
use the depth information for compositing you might end up rendering
that bit in a much higher resolution, combine it with images of the
same resolution, and downsize the results for further compositing.

\subsubsection{No Lights}

%% http://renderman.pixar.com/view/Appnote24

Inspired by RenderMan I decided to have a {\bfseries lighting option}
which is called {\bfseries no\_lights}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_no_lights_P_N.png}
\caption{Beauty, positions in world space, and normals.}
\label{fig:aovs_no_lights_P_N}
\end{figure}

This mode can be used to render {\bfseries extremely fast}. In
RenderMan the {\bfseries defaultsurface} shader is used, which needs
no lights in the scene, because the shading is done entirely based on
the {\bfseries surface normal} and the {\bfseries camera
  position}. Nevertheless such a shader can easily be written by hand
and extended to support AOVs (see below), even for other renderers,
which support some kind of {\bfseries shading language}, or naturally
support AOVs, without the need to write shaders.

The screenshot in figure \ref{fig:aovs_no_lights_P_N} shows you the
resulting {\bfseries beauty} image, which can contain alpha and depth
channels, on the {\bfseries left} side. In the middle you see the
positions (called {\bfseries aov\_P}) and on the right the normals
(called {\bfseries aov\_N}).

The {\bfseries naming convention} comes from \underline{A}rbitrary
\underline{O}utput \underline{V}ariables (short: AOVs\footnote{See
  \url{http://renderman.pixar.com/view/Appnote24}}) and the names used
in the \underline{R}enderMan \underline{S}hading \underline{L}anguage
(RSL). Let me quote from the web page mentioned in the last footnote:

\begin{quote}
  The easiest kind of additional values to export are those which are
  global to the state of each geometric primitive. These are the same
  as the set of external variables available inside each surface
  shader.
\end{quote}

\begin{center}
  \begin{tabular}{| l | l |}
    \hline
    {\bfseries Name} & {\bfseries Meaning} \\ \hline
    \hline
    P & the XYZ coordinates of the surface point \\ \hline
    N & the normal at the surface point \\ \hline
    Ng & the geometric normal at the surface point \\ \hline
    E & the position of the camera \\ \hline
    dPdu, dPdv & the tangent vectors to the surface \\ \hline
    s, t, u, v & the texture coordinate \\ \hline
    du, dv & the stepsize in u and v \\ \hline
    dPdtime & the motion vector for the point P \\ \hline
    Cs, Os & default surface color and opacity \\ \hline
    Ci, Oi & computed surface color and opacity \\ \hline
  \end{tabular}
\end{center}

It's not decided yet how many of those AOVs my exporter will support, but
at least positions and normals are among those.

There is one more thing I would like to mention though. The images
rendered by various renderers are exported in the {\bfseries OpenExr}
file format\footnote{\url{http://www.openexr.org}} which can store
\underline{H}igh \underline{D}ynamic--\underline{R}ange (HDR)
images. Those images do not just contain values in a range
of \begin{math}[0.0, 1.0]\end{math}, but arbitrary floating point
values for each color channel (red, green, and blue). Therefore the
resulting images can not easily be viewed or might look odd in some
image viewers. As an example the positions in the screenshot show
large black, red, green, blue, and yellow areas, whereas the normals
show a lot of blue and red. To see a more meaningful interpretation of
the values you have to view them either in a specialized image viewer,
or you can use a compositing program to load and manipulate those
images.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_normals_comp.png}
\caption{Compositiong network for normals.}
\label{fig:aovs_normals_comp}
\end{figure}

Figure \ref{fig:aovs_normals_comp} showes the {\bfseries Node Editor}
in Blender with a compositing network. On the lower left is a node
which loads the OpenExr image containing the {\bfseries surface
  normals}. To the right of it the {\bfseries beauty} pass is
loaded. Because the normals are normalized (have a length of one
unit), the colors range in \begin{math}[-1.0, 1.0]\end{math}. We need
to transform those values into a visible range \begin{math}[0.0,
    1.0]\end{math}.

So the first image, containing the normals, is separated into three
individual channels for red, green, and blue (RGB). Then each channel
is divided by 2.0, so we end up in a range of \begin{math}[-0.5,
    0.5]\end{math}. This is done on the {\bfseries left side} of the
compositing network.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_positions_comp.png}
\caption{Compositing network for positions in world space.}
\label{fig:aovs_positions_comp}
\end{figure}

On the {\bfseries right side} we add 0.5 to each channel, before
combining all channels into a single RGB color and viewing the
result\footnote{visible in the middle of the screenshot}.

The second image (the beauty pass) is only used to extract the
{\bfseries alpha channel}, which is repsonsible for showing only
geometry which got hit by a ray during rendering.

In a similar way we can visualize the positions in {\bfseries world
  space} by transforming the x, y, and z world coordinates (XYZ) into
a visible range \begin{math}[0.0, 1.0]\end{math}. We have to know the
dimensions of the rendered (visible) scene. In this simple example the
ground plane was centered in the origin and extended 20.0 units in the
positive and negative x and y directions. Therefore we divide by 40.0
(instead of 2.0). You could think of the scene being contained in a
box with one corner being black (20.0 units in the negative x-, y-,
and z-directions), the opposite corner being white. Because the ground
is on level zero the blue channel has the same value of 0.5 everywhere
on ground level, assuming that the z-axis is up. The cylinder close to
the camera is black in the green channel, and the furthest away
cylinder is white. A black edge goes along the ground from lower left
to approximately the middle right of the image. The red channel has a
white cylinder in the back, and the white edge goes approximately from
the center of the image to the right.

It's hard to visualize those things looking at colored images, but it
helps if you can separate individual channels. In the screenshots of
figure \ref{fig:aovs_normals_comp} and \ref{fig:aovs_positions_comp}
there are some red, green, and blue buttons in the GUI of Blender
which allow you to do that.

\subsubsection{Direct Lighting}

%% http://renderman.pixar.com/view/using-secondary-outputs

The {\bfseries lighting option} called {\bfseries direct\_lighting}
possibly supports several algorithms\footnote{e.g. raytracing
  vs. scanline rendering} for some renderers, and might also interpret
the material settings slightly differently to e.g. global
illumination, where energy has to be conserved.

Traditionally RenderMan compatible renderers were used to separate the
ambient, diffuse, and specular contributions (with or without shadows)
for direct illumination. Sometimes only a few lights were used in a
scene, so you could further decide to have such contributions for each
light source separately. Today even RenderMan compatible renderers
support some kind of global illumination, e.g. path tracing, or at
least have some indirect contributions\footnote{See
  \url{http://renderman.pixar.com/view/using-secondary-outputs}}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_direct_diffuse_specular.png}
\caption{Beauty, direct diffuse and direct specular AOVs.}
\end{figure}

It's not decided yet how the workflow will look like for an exporter
which supports several renderers simultaneously, but at least
{\bfseries direct diffuse} and {\bfseries specular} will be available
for those renderers which support AOVs and direct lighting.

\subsubsection{Global Illumination}

For the {\bfseries lighting option} called {\bfseries
  global\_illumination} it makes sense to add at least two more AOVs
for {\bfseries indirect diffuse} and {\bfseries specular}
contributions.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_direct_vs_indirect_diffuse_specular.png}
\caption{Beauty, direct/indirect diffuse, and direct/indirect specular.}
\label{fig:aovs_direct_vs_indirect_diffuse_specular}
\end{figure}

The screenshot in figure
\ref{fig:aovs_direct_vs_indirect_diffuse_specular} shows a more
complex example which has some indirect contributions, and the order
from left to right shows the beauty, the direct diffuse, indirect
diffuse, direct specular, and indirect specular contributions for all
four lights.

\subsubsection{Reflection and Refraction}

Raytracing, where rays can be bend, adds at least two more AOVs for
reflections\footnote{Bending the rays more or less into the {\bfseries
    opposite direction} from where they come from.} and
refractions\footnote{Bending the rays more or less in the {\bfseries
    same direction} from where they come from.}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.27]{img/rvu_blend_aovs_reflection_refraction.png}
\caption{Beauty, reflection, and refraction AOVs.}
\label{fig:aovs_reflection_refraction}
\end{figure}

The screenshot in figure \ref{fig:aovs_reflection_refraction} shows a
close--up of a crystal sphere with the beauty on the left, followed by
reflections of a light emitting sphere, and refractions of the same
sphere inside a room with a window.

\subsubsection{Light Layers/Groups}

Several global illumination renderers support and therefore suggest
the usage of {\bfseries light layers} or {\bfseries light
  groups}. Basically they are either light emitting materials, lights,
or light emitting geometry and can be grouped together.

For example in this scene the contribution of the light emitting
sphere can be separated:

\begin{figure}[h]
\centering
\includegraphics[scale=0.215]{img/rvu_simple_scene_fixture.png}
\caption{Light from light emitting sphere inside the room.}
\label{fig:simple_scene_fixture}
\end{figure}

For the sun and sky simulation there could be a group for both, or you
can divide the contribution further into a sun and sky part. Here is
just the {\bfseries sun}:

\begin{figure}[h]
\centering
\includegraphics[scale=0.215]{img/rvu_simple_scene_sun.png}
\caption{Sunlight only.}
\label{fig:simple_scene_sun}
\end{figure}

And here is the {\bfseries sky} only contribution:

\begin{figure}[h]
\centering
\includegraphics[scale=0.215]{img/rvu_simple_scene_sky.png}
\caption{Contribution of the sky.}
\label{fig:simple_scene_sky}
\end{figure}

The last three screenshots in figures \ref{fig:simple_scene_fixture},
\ref{fig:simple_scene_sun}, and \ref{fig:simple_scene_sky} showed
parts of the user interface of {\bfseries Luxrender} on the left, and
parts of the user interface of {\bfseries Indigo} on the right.

Because those names of the light groups are not known during export
time and are dependent on the current scene, it seems to be the
easiest to group shared materials automatically.

\section{Cafe Scene}

\begin{figure}[h]
\centering
\includegraphics[scale=0.48]{img/Cafe_Scene_800x450.png}
\caption{Two different lighting ``moods'' (cold vs. warm).}
\end{figure}

The second Radiance scene I want to talk about is called {\bfseries
  Cafe Scene} and comes from chapter 2 of the {\bfseries Rendering
  with Radiance}
book\footnote{\url{http://radsite.lbl.gov/radiance/book/index.html}}.

The main idea of the scene is to show how the mood of an image can be
changed by lighting. The same geometry will be illuminated by two
different sets of lights, one with blueish (cold) lights, one with
redish (warm) light sources. Even though global illumination does not
contribute too much to the scene we can compare it's contributions
against the direct lighting results to get a feeling for the impact of
global illumination.

\subsection{Importers}
\label{sec:cafe_scene_importers}

In contrast to the previous scene we will use three main Radiance
files for the geometry, and the two lighting setups, which do load
more scene description files from a library (a sub--directory). This
workflow has impacts on the resulting scene within Blender after the
different files were imported.

\subsubsection{Layers}

\begin{figure}[h]
\centering
\includegraphics[scale=0.405]{img/rvu_blend_cafe_scene_rad.png}
\caption{Layer system showing active and used layers.}
\label{fig:cafe_scene_rad}
\end{figure}

After importing the first Radiance file (called {\bfseries
  cafe\_scene.rad}) you will notice those funny arrows, e.g. pointing
from the floor tiles towards the ceiling\footnote{See figure
  \ref{fig:cafe_scene_rad}.}. We will talk about them in section
\ref{sec:parenting}. But for now I want to direct your attention to
another feature of Blender, which the importer exploits: The usage of
{\bfseries layers}.

At the bottom of the screenshot in figure \ref{fig:cafe_scene_rad} you
will see 20 little buttons, the first one being {\bfseries activated}
(being {\bfseries pressed}) and having a little {\bfseries orange
  circle} in it, to indicate that the {\bfseries current selection} is
found on that layer. The very last layer has a little circle as well,
indicating that there are more scene elements on that layer, but
because the layer button is not pressed those are not visible in the
view port.

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{img/rvu_blend_visible_layers_layer20.png}
\caption{Visible layers filtered in Outliner.}
\label{fig:visible_layers_layer20}
\end{figure}

If we press the {\bfseries layer 20} button we see different parts of
the scene in the view port\footnote{See figure
  \ref{fig:visible_layers_layer20}.}. One feature of the {\bfseries
  Outliner} shown on the right side is the {\bfseries Visible Layers}
option to filter the shown elements and restrict the results to those
elements which are visible in the view port, based on the layer
buttons. We will talk about that a little bit more in section
\ref{sec:groups}.

For now I just want to mention that you can press e.g. the layer 2
button, bring in the lights from {\bfseries lights1.rad} via the
Radiance importer, and do the same on layer 3 for {\bfseries
  lights2.rad}. That way we can easily activate/de--activate one set
of lights before exporting the scene for different renderers.

In general you are free to use all layers for your own purposes, but
{\bfseries layer 20} is kind of special (being used by the {\bfseries
  Radiance importer}).

\subsubsection{Layer 20}

{\bfseries Layer 20} is somehow special for the importer because some
elements might get instanced several times (like the floor tiles) and
the next section will show the usage of groups.

\subsubsection{Groups}
\label{sec:groups}

In figure \ref{fig:visible_layers_layer20} we saw how the {\bfseries
  Outliner} can filter the scene graph for visible layers. In figure
\ref{fig:visible_layers_groups} we used the filter {\bfseries Groups}
on the right side, whereas the left side still uses {\bfseries Visible
  Layers}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{img/rvu_blend_visible_layers_groups.png}
\caption{Visible layers vs. groups filtered in Outliner.}
\label{fig:visible_layers_groups}
\end{figure}

On the left side we see the floor containing 16 elements. Each element
is referring to the same group as you can see in the screenshot for
the first two elements. The group itself contains 4 tiles and we will
find the geometry being used for the tiles on layer 20.

\subsubsection{Parenting}
\label{sec:parenting}

Which brings me back to those funny {\bfseries arrows} mentioned
before. They represent another {\bfseries grouping mechanism}, which
allows {\bfseries parenting} of elements (most of the time an
{\bfseries empty} will be the parent). This will create a hierarchy in
the scene graph and in the outliner you can see that relationship
because the children are collected under the parent (and can be
collapsed).

\subsubsection{Changing the Tesselation for NURBS}
\label{sec:nurbs_tesselation}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{img/rvu_igs_visible_tessellation.png}
\caption{Visible NURBS tessellation.}
\label{fig:visible_tessellation}
\end{figure}

Some renderers support {\bfseries primitives} like spheres, rings,
cylinders, etc. but others do not. In Blender we use a {\bfseries
  naming convention}, so the exporter can decide to use primitives if
they are supported. Most of the time we use {\bfseries NURBS} for the
geometry of primitives, so we can change the {\bfseries tessellation}
for those renderers which do not support primitives (or NURBS). In
figure \ref{fig:visible_tessellation} you can see the edges of
triangles within the image rendered by Indigo.

You will find the parts which make the mirror on the wall in layer 20
and they are called {\bfseries f1}, {\bfseries f2}, {\bfseries f3},
and {\bfseries mirror\_ring}. You can change the resolution
(e.g. {\bfseries Preview U} to 7) to export a finer tessellation or
you could simply edit the shapes in Blender, convert them to meshes,
or re--model the geometry, so that all renderers are forced to use
exactly the same geometry.

\subsection{Exporters}
\label{sec:cafe_scene_exporters}

Technically section \ref{sec:nurbs_tesselation} was already about
exporting the scene in a renderer specific scene description and the
fact that some renderers don't support primitives. I decided to speak
about it in section \ref{sec:cafe_scene_importers} because you have to
know how imported data is stored, as well as how the exporters will
use the stored data. If you have to tweak some parameters, like the
NURBS tesselation, you might be able to do this {\bfseries before} you
export the scene, for all renderers, or you might have to tweak some
parameters later, individually, for a particular renderer.

\begin{figure}[h]
\centering
\includegraphics[scale=0.54]{img/rvu_blend_cafe_scene_comp.png}
\caption{Compositing network to overlay light emitting disks.}
\label{fig:cafe_scene_comp}
\end{figure}

%% WORK

\end{document}
