/* myImager */

imager
myImager()
{
  float xyp[3] = { 1.0, 1.0, 1.0 };
  color bgcolor;
  option("Format", xyp);
  bgcolor = color(xcomp(P) / xyp[0], ycomp(P) / xyp[1], 0);
  Ci = mix(bgcolor, Ci, alpha);
  Oi = 1;
  alpha = 1;
}
