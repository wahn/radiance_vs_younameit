/* mySurface */

surface
mySurface(float Ka = 1, Kd = 0.5, Ks = 0.5, roughness = 0.1;
	  color specularcolor = 1)
{
  /* some of this variables should be shader parameters */
  float dist;
  float inDisk;
  float radius = 0.4;
  float fuzz = 0.1;
  float ss, tt;
  point centre = point(0.5, 0.5, 0.0);
  point here;
  color inside  = color(1, 1, 0);
  color outside = color(1, 0, 0);
  color myCs;
  normal Nf = faceforward(normalize(N), I);
  /* repeat the pattern */
  ss = mod(s*10, 1);
  tt = mod(t*10, 1);
  /* use ss and tt instead of s and t */
  here = point(ss, tt, 0.0);
  /* how far are we away from the centre? */
  dist = distance(centre, here);
  /* are we inside the disk? */
  inDisk = 1 - smoothstep(radius - fuzz, radius + fuzz, dist);
  /* use inside or outside color */
  Oi = Os;
  myCs = Os * mix(outside, inside, inDisk);
  Ci = Os * (myCs * (Ka * ambient() + Kd * diffuse(Nf)) + 
	     specularcolor * Ks * specular(Nf, normalize(-I), roughness));
}
