/* myLight */

light
myLight(float intensity = 1, near = 1, far = 10;
	color lightcolor = 1;
	point from = point "shader" (0,0,0))
{
  float len, brightness;

  illuminate (from)
    {
      len = length(L);
      if (len < near) brightness = 1;
      else if (len > far) brightness = 0;
      else brightness = 1 - (len - near) / (far - near);
      Cl = intensity * lightcolor * brightness;
    }
}
